[![pipeline status](https://gitlab.com/octomy/batch/badges/production/pipeline.svg)](https://gitlab.com/octomy/batch/-/commits/production)
# About batch

<img src="https://gitlab.com/octomy/batch/-/raw/production/design/logo-1024.png" width="20%"/>

Batch is a batch tasks queue and executor in PyPi package form. See also [https://gitlab.com/octomy/batch-base](batch-base) for Docker image based on this.

- Batch is [available on gitlab](https://gitlab.com/octomy/batch).
- Batch is [available in PyPI](https://pypi.org/project/octomy-batch/).

```shell
# Clone git repository
git clone git@gitlab.com:octomy/batch.git

```


```shell
# Install into your current Python environment
pip install octomy-batch

```




